import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/**
 * Chrono
 * Hocine Triki (2020)
 */
class Chrono extends StatefulWidget {
  //const Chrono({ Key key }) : super(key: key);

  _ChronoState cs = new _ChronoState();

  @override
  _ChronoState createState() => cs; //_ChronoState();

  stop()    => cs.stop();
  restart() => cs.restart();
  isActif() => cs._isActif();
}

const Color startColor = Colors.red;
const Color stopColor = Colors.black;

const oneSec = const Duration(milliseconds: 100);
//const oneSec = const Duration(seconds: 1);
class _ChronoState extends State<Chrono> {

  int _t = 0;
  Timer _timer;
  Color _color = stopColor;

  _isActif() {
    return (_timer != null && _timer.isActive);
  }

  void go() {
    if (_isActif()) {
      this.stop();
    } else {
      this.start();
    }
  }

  void start() {

    setState(() {
      _color = startColor;
      this._timer = new Timer.periodic(
        oneSec,
            (Timer timer) => setState(
              () {
            _t = _t + 1;
          },
        ),
      );
    });
  }

  void stop() {

    setState(() {
      _color = stopColor;
      if (_isActif()) {
        _timer.cancel();
      }
    });
  }

  void restart() {
    setState(() {
      if (_isActif()) {
        _timer.cancel();
      }
      this._t = 0;
      this.start();
    });
  }

  /*
  void raz() {
    this.stop();
    this._t = 0;
  } */

  void _reinit() {
    this.stop();
    _t = 0;
  }

  String _getTime() {

    final int heures = (_t / 36000).truncate();

    final double resteMinutes = ((_t - (heures * 36000)) / 10);
    final int minutes = (resteMinutes / 60).truncate();

    final double resteSecondes = resteMinutes - (minutes * 60);
    final int secondes = resteSecondes.truncate();

    final int millisecondes = ((resteSecondes - secondes.roundToDouble()) * 10).ceil();

    final String heure = (heures).toString().padLeft(2, '0');
    final String minute = (minutes).toString().padLeft(2, '0');
    final String seconde = (secondes).toString().padLeft(2, '0');
    final String milliseconde = (millisecondes).toString().padLeft(2, '0');

    return '$heure : $minute : $seconde . $milliseconde';
  }

  @override
  Widget build(BuildContext context) {
    return
      Container(
          margin: const EdgeInsets.all(5),
          child:
          OutlineButton(
            color: Colors.blue,
            padding: EdgeInsets.all(20),
            shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0)),
            child: Text(_getTime()),
            textColor: _color,
            onPressed: () => go(),
            onLongPress: () => _reinit(),
          )
      );
  }
}