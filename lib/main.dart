import 'package:flutter/material.dart';
import 'package:jchrono/chrono.dart';

void main() {
  runApp(MyApp());
}

/**
 * Chrono
 * Hocine Triki
 * 2020
 */
class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: 'JChrono',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'JChrono'),
    );
  }
}

class MyHomePage extends StatefulWidget {

  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with SingleTickerProviderStateMixin {

  AnimationController _animationController;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  List<Chrono> _list = List();

  @override
  void initState() {
    _animationController =
    new AnimationController(vsync: this, duration: Duration(seconds: 1));
    _animationController.repeat();
    super.initState();
  }

  void _showScaffold(String message) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(message),
    ));
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _add() {
    _list.add(new Chrono());
    setState(() {

    });
  }

  void _remove() {

    Chrono c = _list.last;
    c.stop();
    setState(() {
      this._list.removeLast();
    });
  }

  void _startAll() {
    setState(() {
      for (Chrono c in _list) c.restart();
    });
  }

  void _stopAll() {
    setState(() {
      for (Chrono c in _list) c.stop();
    });
  }

  Widget _getRows() {
    return
      (_list.length == 0)
          ?
      Container (
          padding: const EdgeInsets.all(20.0),
          margin: const EdgeInsets.all(20),
          decoration: BoxDecoration(
            borderRadius: new BorderRadius.all(Radius.circular(15)),
            border: Border.all(
              color: Colors.grey,
              width: 1,
            ),
          ),
          child: Column (
            children: <Widget>[
              //Image.asset('assets/images/f.png')
              /*
              Row (
                children: [
                  Container(
                    color: Colors.green,
                    child: new IconButton(
                      icon: new Icon(Icons.search,color: Colors.white,),onPressed: null),
                    ),
                    Text("k")
                ],
              ),*/
              Row (
                children: [
                  CircleAvatar(
                    radius: 20,
                    backgroundColor: Colors.green,
                    child: IconButton(
                      icon: Icon(
                        Icons.add,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Padding (
                    padding: EdgeInsets.only(left: 10),
                    child: Text("Ajouter un nouveau chrono"),
                  )
                ],
              ),
              Row (
                children: [Text("")],
              ),
              Row (
                children: [
                  CircleAvatar(
                    radius: 20,
                    backgroundColor: Colors.pink,
                    child: IconButton(
                      icon: Icon(
                        Icons.remove,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Padding (
                    padding: EdgeInsets.only(left: 10),
                    child: Text("Supprimer un chrono"),
                  )
                ],
              ),
              Row (
                children: [Text("")],
              ),
              Row (
                children: [
                  CircleAvatar(
                    radius: 20,
                    backgroundColor: Colors.blueAccent,
                    child: IconButton(
                      icon: Text("Go", style: TextStyle(fontSize: 11)),
                    ),
                  ),
                  Padding (
                    padding: EdgeInsets.only(left: 10),
                    child: Text("Lancer tous les chrono"),
                  )
                ],
              ),
              Row (
                children: [Text("")],
              ),
              Row (
                children: [
                  CircleAvatar(
                    radius: 20,
                    backgroundColor: Colors.blueAccent,
                    child: IconButton(
                      icon: Text("Stop", style: TextStyle(fontSize: 11)),
                    ),
                  ),
                  Padding (
                    padding: EdgeInsets.only(left: 10),
                    child: Text("Arrêter tous les chrono"),
                  )
                ],
              ),
              Row (
                children: [Text("")],
              ),
              Row (
                children: [
                  Container(
                      child: CircleAvatar(
                          child: Text('00:00', style: TextStyle(fontSize: 11)),
                          foregroundColor: Colors.black,
                          backgroundColor: Colors.white,
                      ),
                      padding: const EdgeInsets.all(1.0), // borde width
                      decoration: new BoxDecoration(
                        color: Colors.grey, // border color
                        shape: BoxShape.circle,
                      )
                  ),
                  Padding (
                    padding: EdgeInsets.only(left: 10),
                    child: Text("Lancer ou stopper un chrono\nRester appuyé  pour\nréinitialiser le chrono"),
                  )
                ],
              ),

            ],
          ))
          :
      Wrap (
        children: <Widget>[
          for (var c in _list) c
        ],
      );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container (
                child: Column (
                  children: <Widget>[
                    this._getRows()
                    //for (var c in _list) c
                  ],
                )
            )
          ],
        ),
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          new Visibility(
              visible: (_list.length > 0),
              child: Padding(
                  padding: const EdgeInsets.only(right: 8),
                  child: SizedBox(
                      height: 40, width: 40,
                      child: FloatingActionButton(child: Text("Go"),
                        tooltip: "Démarre tous les chronos",
                        onPressed: _startAll, backgroundColor: Colors.blueAccent,shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16.0))),)))
          ),
          new Visibility(
              visible: (_list.length > 0),
              child: Padding(
                  padding: const EdgeInsets.only(right: 8),
                  child: SizedBox(
                      height: 40, width: 40,
                      child: FloatingActionButton(child: Text("Stop"),
                        tooltip: "Stop tous les chronos",
                        onPressed: _stopAll, backgroundColor: Colors.blueAccent,shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16.0))),)))
          ),
          new Visibility(
            visible: (_list.length > 0),
            child: SizedBox(
                height: 40, width: 40,
                child: FloatingActionButton(child: Icon(Icons.remove),onPressed: _remove, backgroundColor: Colors.pink,shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16.0))),
                )
            ),),
          new Visibility(
              visible: (_list.length > 0 && _list.length < 10),
              child: Padding(
                  padding: const EdgeInsets.only(left: 8),
                  child: SizedBox(
                    height: 40, width: 40,
                    child: FloatingActionButton(child: Icon(Icons.add),onPressed: _add,
                      backgroundColor: Colors.green,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16.0))),),)
              )
          ),
          new Visibility(
            visible: (_list.length == 0),
            child: SizedBox(
                height: 40, width: 40,
                child: FloatingActionButton(child: Text("?"),onPressed: (){
                  _showScaffold("Hocine Triki (2020)");
                }, backgroundColor: Colors.yellow, foregroundColor: Colors.black, shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16.0))),
                )
            ),),
          new Visibility(
              visible: (_list.length == 0),
              child: Padding(
                  padding: const EdgeInsets.only(left: 8),
                  child: SizedBox(
                    height: 40, width: 40,
                    child: FadeTransition(
                      opacity: _animationController,
                      child: FloatingActionButton(child: Text("Start", style: TextStyle(fontWeight: FontWeight.bold)),
                        onPressed: _add,
                        backgroundColor: Colors.white, foregroundColor: Colors.black,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16.0))),),
                    )
                  ),
    /*
                    child: FloatingActionButton(child: Text("Start"),onPressed: _add,
                      backgroundColor: Colors.yellow, foregroundColor: Colors.black,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16.0))),),)
    */
              )
          ),

        ],),
    );
  }
}


